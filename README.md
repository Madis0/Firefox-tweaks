# CSS tweaks for Firefox

A list of tweaks I have made for the [Firefox browser](https://firefox.com). Some tweaks may not be compatible with each other so test, read comments and adjust code as needed. Tweaks where I am the sole owner are licenced under MIT, others are GPLv3.

For more tweaks, check out [Timvde's UserChrome tweaks](https://github.com/Timvde/UserChrome-Tweaks) and [r/FirefoxCSS](https://www.reddit.com/r/FirefoxCSS/). I used to contribute to Timvde's repository, but nowadays I am on my own in order to manage the updates faster.

To see how to install the styles, check out [Installing.md](https://gitlab.com/Madis0/Firefox-tweaks/blob/master/Installing.md).