## userChrome (recommended)

It is recommended to manually [create an userChrome.css file](https://www.userchrome.org/how-create-userchrome-css.html) and copy styles into it. This is what you need to use if you want to preview styles too.

### Pre-setup
1. Open `about:profiles`.
2. Find the section that claims to be "in use" and press the "Show" button on it's Root Directory. Close Firefox.
3. Create a folder called "chrome" (no caps) and open it.
4. Open a text editor and create a text file **userChrome.css** (make sure it doesn't end with `.txt`!), write "test" to it.
5. Open Firefox and press `Ctrl` + `Shift` + `I` on Win/Linux or `⌘⌥I` on Mac.
6. Click on the three dots that appear in the right corner, click on **Settings**.
7. Scroll down to Advanced Settings and check the settings *"Enable browser chrome and add-on debugging toolboxes"* and *"Enable remote debugging"*.
8. Close the developer tools panel, open `about:config` and set `toolkit.legacyUserProfileCustomizations.stylesheets` to `true`.
8. You're done, follow the next tutorial

### Testing/installing styles

1. Press `Ctrl` + `Alt` + `Shift` + `I` on Win/Linux or `⌘⌥⇧I` on Mac 
2. Click on the tab *Style Editor*, choose file *userChrome.css* on the sidebar. If you haven't done this before, it should contain "test" in it.
3. Choose the style you want to from this repository, click **Open raw** and copy the code. 
4. Scroll down on the development tools window textbox, paste the code.
5. You should see the style being applied. If you like what you see, you can click Save, otherwise it will disappear after restarting the browser.

## reStyle (easier but may not work)

Potentially easier way of installing and managing styles is with [reStyle](https://addons.mozilla.org/en-US/firefox/addon/re-style/). However, the extension has not been managed for years, so it is unknown if this method still works.
Here's how to do it:

### Pre-setup

1. Install [reStyle](https://addons.mozilla.org/en-US/firefox/addon/re-style/)
2. Install [NativeExt](https://addons.mozilla.org/en-US/firefox/addon/native-ext/)
3. From the NativeExt options page that opened, click **download**.
4. Double-click the file you downloaded. You should see a confirmation message.
5. Go back to NativeExt options and click **Apply** under External extensions. You should see a notification titled "Done".
6. Open reStyle options and click on Setup on the left.
7. Click `Request Permission` and allow the prompt that appears.
8. Close the settings pages, open `about:config` and set `toolkit.legacyUserProfileCustomizations.stylesheets` to `true`.
8. You're done, follow the next tutorial

### Installing styles

1. Find a style you like by browsing the folders and clicking the link ending with **.css**.
2. Click the reStyle magic wand button on the toolbar, then `Add style`
3. Repeat the process for any other file you want to add
4. Restart Firefox to see the changes!
